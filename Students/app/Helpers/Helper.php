<?php

namespace App\Helpers;

class Helper
{
    static function separateFullName($fullName)
    {
        $fname = $lname = '';
        $names = explode(' ', $fullName);

        if (array_key_exists(0, $names)) {
            $fname = $names[0];
            array_shift($names);
        }

        if (!empty($names)) {
            if (array_key_exists(0, $names)) {
                $lname = $names[0];
                array_shift($names);
            }
        }
        $name[0] = $fname;
        $name[1] = $lname;

        return $name;
    }
}
