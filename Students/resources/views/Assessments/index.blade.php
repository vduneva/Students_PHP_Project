@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row">

            <div class="container">
                @if (\Session::has('message'))
                    <div class="alert alert-info">{{\Session::get('message') }}</div>
                @endif

                <div class="row" style="float: right">
                    <a class="icon-btn btn-success non-decorated " href="{{url('assessments/create/')}}">
                        <span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-success"></span>
                        Добави
                    </a>
                </div>
            </div>
        </div>

        <div class="row col-6 text-center">
            <form action="{{action('StudentAssessmentsController@index')}}" method="get">
                {{ csrf_field() }}
                <fieldset>
                    <label for="studentName">Име: </label>
                    <input id="studentName" name="studentName" type="text" size="30"/>
                    <button type="submit" class="search-btn btn btn-sm">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    <br/>
                    <div style="margin-right: 228px;">
                        <label for="subjectID">Дисциплина: </label>
                        <select id="subjectID" name="subjectID">

                            @foreach ($subjects->sortBy('id') as $subject)
                                <option value="{{ $subject->id }}">
                                    {{ $subject->name }}
                                </option>
                            @endforeach
                        </select><br/>
                    </div>
                </fieldset>
            </form>
        </div>

        <br class="clearfix"/>
        <div class="row col-5">
            @if ($assessments->count() > 0)
                <table class="table table-stripped table-responsive table-bordered">
                    <thead class="th-dark justify-content-center">

                    <tr>
                        <td>№</td>
                        <td>Име, Фамилия</td>
                        <td>Дисциплина</td>
                        <td>Оценка</td>
                        <td>Операции</td>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($assessments as $assessment)
                        <tr>
                            <td>{{ $startIndex }}</td>
                            <td>{{ $assessment->fname }} {{ $assessment->lname }}</td>
                            <td>{{ $assessment->name }}</td>
                            <td>{{ \App\Models\StudentAssessment::showGrade($assessment->assessment)}}</td>
                            <td>
                                <a href="{{ URL::to('assessments/' .$assessment->id  .'/edit')}}">
                                    <button class="edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                </a>

                                <form action="{{action('StudentAssessmentsController@destroy', $assessment->id )}}"
                                      method="post" class="delete_form">
                                    {{csrf_field()}}
                                    <input name="_method" type="hidden" value="DELETE"/>
                                    <button type="submit" class="delete">
                                        <span class="glyphicon glyphicon-trash" style="color: red"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @php($startIndex++)
                    @endforeach
                    @else
                        <div class="row col-6 text-center">Няма намерени резултати!
                            <a href="{{url('assessments/')}}">
                                <span class="glyphicon glyphicon-circle-arrow-left"></span></a></div>
                    @endif
                    </tbody>
                </table>
                @if( $assessments->count() > 0)
                    {{ $assessments->links() }}
                @endif
        </div>
    </div>
@endsection
