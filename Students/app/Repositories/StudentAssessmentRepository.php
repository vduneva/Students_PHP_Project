<?php

namespace App\Repositories;

use App\Models\StudentAssessment;
use Torann\LaravelRepository\Repositories\AbstractRepository;

class StudentAssessmentRepository extends AbstractRepository
{
    protected $model = StudentAssessment::class;

}

