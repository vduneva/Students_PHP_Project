<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Repositories\StudentAssessmentRepository;
use App\Repositories\StudentRepository;
use App\Repositories\SubjectRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentAssessmentsController extends Controller
{
    protected $assessmentRepository;
    protected $studentRepository;
    protected $subjectRepository;

    public function __construct(StudentAssessmentRepository $assessmentRepository, StudentRepository $studentRepository, SubjectRepository $subjectRepository)
    {
        $this->middleware('auth');
        $this->assessmentRepository = $assessmentRepository;
        $this->studentRepository = $studentRepository;
        $this->subjectRepository = $subjectRepository;
    }

    private function rules($subjectID)
    {
        $subject = $this->subjectRepository->findOrFail($subjectID);

        return [
            'studentFN' => 'required|integer|exists:students,fnumber',
            'subject_id' => 'required|integer|exists:subjects,id',
            'workload_lectures' => 'required|integer|min:0|max:' . $subject['workload_lectures'],
            'workload_exercises' => 'required|integer|min:0|max:' . $subject['workload_exercises'],
            'assessment' => 'required|numeric|min:2|max:6'
        ];
    }

    private function errors($subjectID)
    {
        $subject = $this->subjectRepository->findOrFail($subjectID);

        return [
            'studentFN.required' => "Моля попълнете полето.",
            'studentFN.integer' => "Моля въведете студентски номер.",
            'studentFN.exists' => "Моля въведете съществуващ студентски номер.",

            'subject_id.required' => "Моля попълнете полето.",
            'subject_id.integer' => "Моля въведете номера на предмета.", //???
            'subject_id.exists' => "Моля въведете съществуващ предмет.",

            'workload_lectures.required' => "Моля попълнете полето.",
            'workload_lectures.integer' => "Моля въведете число.",
            'workload_lectures.min' => "Миниламният брой лекции е 0.",
            'workload_lectures.max' => "Максималният брой лекции е " . $subject['workload_lectures'],

            'workload_exercises.required' => "Моля попълнете полето.",
            'workload_exercises.integer' => "Моля въведете число.",
            'workload_exercises.min' => "Миниламният брой упражнения е 0.",
            'workload_exercises.max' => "Максималният брой упражнения е " . $subject['workload_exercises'],

            'assessment.required' => "Моля попълнете полето.",
            'assessment.integer' => "Моля въведете число",
            'assessment.min' => "Най-ниската оценка е 2.",
            'assessment.max' => "Най-високата оценка е 6."
        ];
    }

    private $name;
    private $subjectId;

    private function showAssessments()
    {
        return $this->assessmentRepository->addScopeQuery(
            function ($query) {
                return $query->join('students', 'students_assessments.student_id', '=', 'students.id')
                    ->join('subjects', 'students_assessments.subject_id', '=', 'subjects.id')
                    ->select('students_assessments.*', 'students.fname', 'students.lname', 'subjects.name');
            });
    }

    private function subjectFilter()
    {
        return $this->showAssessments()->addScopeQuery(function ($query) {
            return $query->where('subject_id', 'like', $this->subjectId);
        });
    }

    private function studentNameFilter()
    {
        return $this->showAssessments()->addScopeQuery(function ($query) {
            return $query->where('fname', 'like', '%' . $this->name[0] . '%');
        })->addScopeQuery(function ($query) {
            return $query->where('lname', 'like', '%' . $this->name[1] . '%');
        });
    }

    public function index(Request $request)
    {
        $studentName = $request->get('studentName', null);
        if (!is_null($studentName)) {
            $this->name = Helper::separateFullName($studentName);
        }
        $this->subjectId = $request->get('subjectID', 0);
        $perPage = 10;
        $startIndex = (($request->get('page', 1) - 1) * $perPage) + 1;

        $subjects = $this->subjectRepository->all();
        $all = $this->subjectRepository->makeModel();
        $all['id'] = 0;
        $all['name'] = "Всички";
        $subjects->push($all);

        $assessments = $this->showAssessments()->paginate($perPage);
        if (!empty($request->all())) {
            switch ($request->except('_token')) {
                case is_null($studentName) && ($this->subjectId != 0):
                    $assessments = $this->subjectFilter()->paginate($perPage)->appends($request->only('subjectID'));
                    break;
                case !is_null($studentName) && ($this->subjectId == 0):
                    $assessments = $this->studentNameFilter()->paginate($perPage)->appends($request->only('studentName'));
                    break;
                case !is_null($studentName) && ($this->subjectId != 0):
                    $assessments = $this->subjectFilter()->addScopeQuery(function ($query) {
                        return $query->where('fname', 'like', '%' . $this->name[0] . '%');
                    })->addScopeQuery(function ($query) {
                        return $query->where('lname', 'like', '%' . $this->name[1] . '%');
                    })->paginate($perPage)->appends($request->all());
                    break;
                default:
                    $assessments = $this->showAssessments()->paginate($perPage)->appends($request->all());;
                    break;
            }
        }
        return View('assessments.index', compact('assessments', 'subjects', 'startIndex'));
    }

    public function create()
    {
        $students = $this->studentRepository->all();
        $subjects = $this->subjectRepository->all();

        return view('assessments.create', compact('students', 'subjects'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules($request->get('subject_id')), $this->errors($request->get('subject_id')));

        if ($validator->fails()) {
            return redirect('assessments/create')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            try {
                $student = $this->studentRepository->findWhere(['fnumber' => $request->get('studentFN')], ['*'], 'like');
                $studentName = $student[0]['fname'] . " " . $student[0]['lname'];

                $assessment = $this->assessmentRepository->makeModel()->make($request->except('studentFN'));
                $assessment['student_id'] = $student[0]['id'];
                $this->assessmentRepository->create($assessment->toArray());
                return redirect('assessments')->with('message', 'Успешно добавихте нова оценка на ' . $studentName);
            } catch (\Exception $е) {
                return redirect('assessments/create')->with('message', 'Моля проверете коректността на въведените от Вас данни.');
            }
        }
    }

    public function edit($id)
    {
        $assessment = $this->assessmentRepository->findBy('id', $id, ['*']);

        if (is_null($assessment)) {
            return redirect('/assessments');
        }
        return view('assessments.edit', compact('assessment'));
    }

    public function update(Request $request, $id)
    {
        $assessment = $this->assessmentRepository->findBy('id', $id, ['*']);

        if (is_null($assessment)) {
            return redirect('/assessments');
        }

        $validator = Validator::make($request->all(), $this->rules($request->get('subject_id')), $this->errors($request->get('subject_id')));
        if ($validator->fails()) {
            return redirect('assessments/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            try {
                $differences = 0;
                if ($assessment['workload_lectures'] != $request->get('workload_lectures')) {
                    $differences++;
                }
                if ($assessment['workload_exercises'] != $request->get('workload_exercises')) {
                    $differences++;
                }
                if ($assessment['assessment'] != $request->get('assessment')) {
                    $differences++;
                }
                if ($differences > 0) {
                    $this->assessmentRepository->update($assessment, $request->except('studentName', 'studentFN', 'subject_id'));
                    return redirect('/assessments')->with('message', 'Успешно обновихте информацията за ' . $request->get('studentName'));
                } else {
                    return redirect('/assessments');
                }
            } catch (\Exception $е) {
                return redirect('assessments/' . $id . '/edit')->with('message', 'Моля проверете коректността на въведените от Вас данни.')->withInput($request->all());
            }
        }
    }

    public function destroy($id)
    {
        try {
            $assessment = $this->assessmentRepository->findOrFail($id, ['*']);
            $this->assessmentRepository->delete($assessment);
            return redirect('/assessments')->with('message', 'Успешно изтрихте избраната оценка.');
        } catch
        (\Exception $е) {
            return redirect('/assessments')->with('message', 'Моля проверете коректността на въведените от Вас данни.');
        }
    }
}
