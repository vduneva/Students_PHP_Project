<?php

namespace App\Repositories;

use App\Models\Student;
use Torann\LaravelRepository\Repositories\AbstractRepository;

class StudentRepository extends AbstractRepository
{
    protected $model = Student::class;
}
