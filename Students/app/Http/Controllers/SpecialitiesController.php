<?php

namespace App\Http\Controllers;

use App\Models\Speciality;
use App\Repositories\SpecialityRepository;
use App\Repositories\StudentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SpecialitiesController extends Controller
{
    protected $specialityRepository;
    protected $studentRepository;

    public function __construct(SpecialityRepository $specialityRepository, StudentRepository $studentRepository)
    {
        $this->middleware('auth');
        $this->specialityRepository = $specialityRepository;
        $this->studentRepository = $studentRepository;
    }

    private function rules($id)
    {
        return [
            'name' => 'required|max:50|unique:specialities,name,' . $id,
            'name_short' => 'required|max:8|unique:specialities,name_short,' . $id
        ];
    }

    private $errors = [
        'name.required' => "Моля попълнете полето.",
        'name.unique' => 'Името се използва.',
        'name.max' => 'Дължината на името не трябва да е повече от 50 символа.',

        'name_short.required' => "Моля попълнете полето.",
        'name_short.unique' => 'Абревиатурата се използва.',
        'name_short.max' => 'Дължината на абревиатурата не трябва да е повече от 8 символа.'
    ];
    private $name;

    public function index(Request $request)
    {
        $specialityName = $request->get('specialityName', null);
        $perPage = 4;
        $startIndex = (($request->get('page', 1) - 1) * $perPage) + 1;
        $specialities = $this->specialityRepository->paginate($perPage);

        if (!is_null($specialityName)) {
            $this->name = $specialityName;
            $specialities = $this->specialityRepository->addScopeQuery(function ($query) {
                return $query->where('name', 'like', $this->name . '%');
            })->paginate($perPage)->appends($request->only('specialityName'));
        }
        return view('specialities.index', compact('specialities', 'startIndex'));
    }

    public function create()
    {
        return view('specialities.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules($request->get('id')), $this->errors);

        if ($validator->fails()) {
            return redirect('specialities/create')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            try {
                $this->specialityRepository->create($request->all());
                return redirect('specialities')->with('message', 'Успешно добавихте нова специалност.');
            } catch (\Exception $е) {
                return redirect('specialities/create')->with('message', 'Моля проверете коректността на въведените от Вас данни.');
            }
        }
    }

    public function edit($id)
    {
        $speciality = $this->specialityRepository->findBy('id', $id, ['*']);

        if (is_null($speciality)) {
            return redirect('/specialities');
        }
        return view('specialities.edit', compact('speciality'));
    }

    public function update(Request $request, $id)
    {

        $speciality = $this->specialityRepository->findBy('id', $id, ['*']);

        if (is_null($speciality)) {
            return redirect('/specialities');
        }

        $validator = Validator::make($request->all(), $this->rules($id), $this->errors);
        if ($validator->fails()) {

            return redirect('specialities/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            try {
                $differences = 0;
                if ($speciality['name'] != $request->get('name')) {
                    $differences++;
                }
                if ($speciality['name_short'] != $request->get('name_short')) {
                    $differences++;
                }
                if ($differences > 0) {
                    $this->specialityRepository->update($speciality, $request->all());
                    return redirect('/specialities')->with('message', 'Успешно обновихте информацията за ' . $speciality['name']);
                } else {
                    return redirect('/specialities');
                }
            } catch (\Exception $е) {
                return redirect('specialities/' . $id . '/edit')->with('message', 'Моля проверете коректността на въведените от Вас данни.')->withInput($request->all());
            }
        }
    }

    public function destroy($id)
    {
        try {
            $speciality = $this->specialityRepository->findOrFail($id, ['*']);
            $student = $this->studentRepository->findWhere(['speciality_id' => $id], ['id'], 'like');
            $name = $speciality['name'];

            if ($student->count() > 0) {
                return redirect('/specialities')->with('message', $name . ' се използва и не може да бъде изтрит/а.');
            }

            $this->specialityRepository->delete($speciality);
            return redirect('/specialities')->with('message', 'Успешно изтрихте специалност ' . $name);
        } catch (\Exception $e) {
            return redirect('/specialities')->with('message', 'Моля проверете коректността на въведените от Вас данни.');
        }
    }
}
