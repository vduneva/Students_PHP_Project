@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="row col-6 text-center">
                <form action="" method="get">
                    {{ csrf_field() }}
                    <fieldset>
                        <legend>Търсене</legend>
                        <label for="studentName" style="margin-left: 54px;">Име: </label>
                        <input id="studentName" name="studentName" type="text" size="30"/>
                        <button type="submit" class="search-btn btn btn-sm">
                            <span class="glyphicon glyphicon-search"></span>
                        </button><br/>

                        <div style="margin-right: 148px;">
                            <label for="courseID">Курс: </label>
                            <select id="courseID" name="courseID">
                                @foreach ($courses->sortBy('id') as $course)
                                    <option value="{{ $course->id }}">
                                        {{ $course->name }}
                                    </option>
                                @endforeach
                            </select><br/>
                        </div>

                        <div style="margin-right: 64px;">
                            <label for="specialityID">Специалност: </label>
                            <select id="specialityID" name="specialityID">
                                @foreach ($specialities->sortBy('id') as $speciality)
                                    <option value="{{ $speciality->id }}">
                                        ({{ $speciality->name_short }}) {{ $speciality->name }}
                                    </option>
                                @endforeach
                            </select>&nbsp;
                        </div>
                    </fieldset>
                </form>
            </div>

            <br class="clearfix"/>
            <div class="row col-12">
                @if($students->count() > 0)
                    <table class="table table-stripped table-responsive table-bordered">
                        <thead class="th-dark justify-content-center">
                        <tr>
                            <td rowspan="2"></td>
                            <td colspan="2" rowspan="2"></td>
                        </tr>
                        <tr class="justify-content-center">
                            @foreach($subjects as $subject)
                                <td colspan="3" class="text-center"> {{ $subject->name }} </td>
                            @endforeach
                            <td colspan="3" class="text-center">Общо</td>
                        </tr>
                        <tr>
                            <td>№</td>
                            <td>Име, Фамилия</td>
                            <td>Курс</td>
                            @foreach($subjects as $subject)
                                <td>Лекции</td>
                                <td>Упражнения</td>
                                <td>Оценка</td>
                            @endforeach
                            <td>Лекции</td>
                            <td>Упражнения</td>
                            <td>Успех</td>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($students as $student)
                            <tr>
                                <td>{{ $startIndex }}</td>
                                <td>{{ $student->fname }} {{ $student->lname }} ({{ $student->fnumber }})</td>
                                <td>{{ $student->name }}, {{ $student->name_short }}, ({{ $student->education_form }})
                                </td>
                                @foreach($subjects as $key => $subject)
                                    @php
                                        $lectures_s = explode(',', $student->lectures_s);
                                        $lectures_sb = explode(',', $student->lectures_sb);
                                        $exercises_s = explode(',', $student->exercises_s);
                                        $exercises_sb = explode(',', $student->exercises_sb);
                                        $assessment = explode(',', $student->assessment);

                                            if (!array_key_exists($key, $lectures_s)) {
                                               $lectures_s[$key] = 0;
                                            }
                                            if (!array_key_exists($key, $lectures_sb)) {
                                               $lectures_sb[$key] = 0;
                                            }
                                             if (!array_key_exists($key, $exercises_s)) {
                                                $exercises_s[$key] = 0;
                                             }
                                             if (!array_key_exists($key, $exercises_sb)) {
                                                 $exercises_sb[$key]=0;
                                             }
                                             if (!array_key_exists($key, $assessment)) {
                                                  $assessment[$key]='';
                                             }

                                    @endphp
                                    <td><span class="text-danger">{{ $lectures_s[$key] }}</span>
                                        ({{ $lectures_sb[$key] }})
                                    </td>
                                    <td><span class="text-danger">{{ $exercises_s[$key] }}</span>
                                        ({{ $exercises_sb[$key] }})
                                    </td>
                                    <td>{{ App\Models\StudentAssessment::showGrade($assessment[$key]) }}</td>
                                @endforeach
                                <td><span class="text-danger">{{ $student->lectures_total_s }}</span>
                                    ({{ $student->lectures_total_sb }})
                                </td>
                                <td><span class="text-danger">{{ $student->exercises_total_s }}</span>
                                    ({{ $student->exercises_total_sb }})
                                </td>
                                <td>{{ App\Models\StudentAssessment::showGrade(number_format($student->avg_assessment, 2)) }}</td>

                            </tr>
                            @php($startIndex++)
                        @endforeach
                        @else
                            <div class="row col-6 text-center">Няма намерени резултати! <a href="{{url('home/')}}">
                                    <span class="glyphicon glyphicon-circle-arrow-left"></span></a></div>
                        @endif
                        </tbody>
                    </table>
                    @if( $students->count() > 0)
                        {{ $students->links() }}
                    @endif
            </div>
        </div>
    </div>
@endsection


