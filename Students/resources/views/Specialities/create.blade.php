@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row">
            @if (\Session::has('message'))
                <div class="alert alert-info">{{\Session::get('message') }}</div>
            @endif

            <div class="row col-6 text-center">
                <form action="{{url('specialities')}}" method="post">
                    {{csrf_field()}}
                    <fieldset>
                        <div>
                            <legend>Добавяне на специалност</legend>
                            <label for="name">Пълно име: </label>
                            <input id="name"  name="name" size="30" maxlength="50" required value="{{ old('name') }}"/>
                            <br/>
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        </div>

                        <div style="margin-right: 118px;">
                            <label for="name_short">Абревиатура </label>
                            <input id="name_short"  name="name_short" size="15" maxlength="8" required value="{{ old('name_short') }}"/>
                            <br/>
                            <small class="text-danger">{{ $errors->first('name_short') }}</small>
                        </div>

                        <div style="margin-left: 160px;">
                            <a href="{{ URL::to('specialities')}}" class="btn btn-warning">Откажи</a>
                            <input type="submit" class="btn btn-primary" value="Добави"/>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection