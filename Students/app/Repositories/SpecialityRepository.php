<?php

namespace App\Repositories;

use Torann\LaravelRepository\Repositories\AbstractRepository;
use App\Models\Speciality;

class SpecialityRepository extends AbstractRepository
{
    protected $model = Speciality::class;
}
