<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentAssessment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'students_assessments';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'student_id', 'subject_id', 'assessment', 'workload_lectures', 'workload_exercises',
    ];

    /**
     *
     * Get the students for assessments
     *
     */
    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    /**
     *
     * Get the subjects for assessments
     *
     */
    public function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }

    public static function showGrade($value) {
        $grade = '';
        if($value == '')
        {
            return $grade = '---';
        }

        switch($value) {
            case ($value < 3.00):
                $grade = sprintf("Слаб (%s)", $value);
                break;

            case (($value >= 3.00) && ($value <= 3.49)):
                $grade = sprintf("Среден (%s)", $value);
                break;

            case (($value >= 3.50) && ($value <= 4.49)):
                $grade = sprintf("Добър (%s)", $value);
                break;

            case (($value >= 4.50) && ($value <= 5.49)):
                $grade = sprintf("Мн. добър (%s)", $value);
                break;

            case (($value >= 5.50) && ($value <= 6.00)):
                $grade = sprintf("Отличен (%s)", $value);
                break;
        }

        return $grade;
    }
}
