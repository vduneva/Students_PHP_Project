@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row">
            @if (\Session::has('message'))
                <div class="alert alert-info">{{\Session::get('message') }}</div>
            @endif

            <div class="row col-6 text-center">
                <form action="{{url('courses')}}" method="post">
                    {{csrf_field()}}
                    <fieldset>
                        <div>
                            <legend>Добавяне на курс</legend>
                            <label for="name">Име на курс: </label>
                            <input id="name"  name="name" size="30" maxlength="20" required value="{{ old('name') }}"/>
                            <br/>
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        </div>

                        <div style="margin-left: 160px;">
                            <a href="{{ URL::to('courses')}}" class="btn btn-warning">Откажи</a>
                            <input type="submit" class="btn btn-primary" value="Добави"/>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection