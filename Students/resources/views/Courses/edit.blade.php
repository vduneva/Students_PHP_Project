@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row">
            @if (\Session::has('message'))
                <div class="alert alert-info">{{\Session::get('message') }}</div>
            @endif

            <div class="row col-6 text-center">
                <form action="{{action('CoursesController@update',$course->id)}}" method="post">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="PATCH"/>
                    <fieldset>
                        <legend>Редактиране на курс</legend>
                        <div>
                            <label for="name">Име: </label>
                            <input id="name" name="name" type="text" size="30" maxlength="20" required
                                   value="{{$course->name}}"/><br/>
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        </div>

                        <div style="margin-left: 88px;">
                            <a href="{{ URL::to('courses')}}" class="btn btn-warning">Откажи</a>

                            <input type="submit" class="btn btn-primary" value="Редактирай"/>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection