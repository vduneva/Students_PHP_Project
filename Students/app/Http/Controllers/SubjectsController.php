<?php

namespace App\Http\Controllers;

use App\Repositories\StudentAssessmentRepository;
use App\Repositories\SubjectRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubjectsController extends Controller
{
    protected $subjectRepository;
    protected $assessmentRepository;

    public function __construct(SubjectRepository $subjectRepository, StudentAssessmentRepository $assessmentRepository)
    {
        $this->middleware('auth');
        $this->subjectRepository = $subjectRepository;
        $this->assessmentRepository = $assessmentRepository;
    }

    private function rules($id)
    {
        return [
            'name' => 'required|max:50|unique:subjects,name,' . $id,
            'workload_lectures' => 'required|integer|min:1|max:200',
            'workload_exercises' => 'required|integer|min:1|max:200'
        ];
    }

    private $errors = [
        'name.required' => "Моля попълнете полето.",
        'name.unique' => 'Името се използва.',
        'name.max' => 'Дължината на името не трябва да е повече от 50 символа.',

        'workload_lectures.required' => "Моля попълнете полето.",
        'workload_lectures.integer' => "Моля въведете число",
        'workload_lectures.min' => "Миниламният брой лекции е 1.",
        'workload_lectures.max' => "Максималният брой лекции е 200.",

        'workload_exercises.required' => "Моля попълнете полето.",
        'workload_exercises.integer' => "Моля въведете число",
        'workload_exercises.min' => "Миниламният брой упражнения е 1.",
        'workload_exercises.max' => "Максималният брой упражнения е 200."
    ];
    private $name;

    public function index(Request $request)
    {
        $subjectName = $request->get('subjectName', null);
        $perPage = 4;
        $startIndex = (($request->get('page', 1) - 1) * $perPage) + 1;
        $subjects = $this->subjectRepository->paginate($perPage);

        if (!is_null($subjectName)) {
            $this->name = $subjectName;
            $subjects = $this->subjectRepository->addScopeQuery(function ($query) {
                return $query->where('name', 'like', $this->name . '%');
            })->paginate($perPage)->appends($request->only('subjectName'));
        }
        return view('subjects.index', compact('subjects', 'startIndex'));
    }

    public function create()
    {
        return view('subjects.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules($request->get('id')), $this->errors);

        if ($validator->fails()) {
            return redirect('subjects/create')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            try {
                $this->subjectRepository->create($request->all());
                return redirect('subjects')->with('message', 'Успешно добавихте нова дисциплина.');
            } catch (\Exception $е) {
                return redirect('subjects/create')->with('message', 'Моля проверете коректността на въведените от Вас данни.');
            }
        }
    }

    public function edit($id)
    {
        $subject = $this->subjectRepository->findBy('id', $id, ['*']);

        if (is_null($subject)) {
            return redirect('/subjects');
        }
        return view('subjects.edit', compact('subject'));
    }

    public function update(Request $request, $id)
    {
        $subject = $this->subjectRepository->findBy('id', $id, ['*']);
        if (is_null($subject)) {
            return redirect('/subjects');
        }

        $validator = Validator::make($request->all(), $this->rules($id), $this->errors);
        if ($validator->fails()) {
            return redirect('subjects/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            try {
                $differences = 0;
                //TODO:to find better way to find out if the model is updated!!!
                if ($subject['name'] != $request->get('name')) {
                    $differences++;
                    // $subject->setAttribute('name', $request->get('name'));
                }
                if ($subject['workload_lectures'] != $request->get('workload_lectures')) {
                    $differences++;
                    // $subject->setAttribute('workload_lectures', $request->get('workload_lectures'));
                }
                if ($subject['workload_exercises'] != $request->get('workload_exercises')) {
                    $differences++;
                    // $subject->setAttribute('workload_exercises', $request->get('workload_exercises'));l
                }
                if ($differences > 0) {
                    $this->subjectRepository->update($subject, $request->all());
                    return redirect('/subjects')->with('message', 'Успешно обновихте информацията за ' . $subject['name']);
                } else {
                    return redirect('/subjects');
                }
            } catch (\Exception $е) {
                return redirect('subjects/' . $id . '/edit')->with('message', 'Моля проверете коректността на въведените от Вас данни.')->withInput($request->all());
            }
        }
    }

    public function destroy($id)
    {
        try {
            $subject = $this->subjectRepository->findOrFail($id, ['*']);
            $assessments = $this->assessmentRepository->findWhere(['subject_id' => $id], ['id'], 'like');
            $name = $subject['name'];

            if ($assessments->count() > 0) {
                return redirect('/subjects')->with('message', $name . ' се използва и не може да бъде изтрит/а.');
            }

            $this->subjectRepository->delete($subject);
            return redirect('/subjects')->with('message', 'Успешно изтрихте дисциплина  ' . $name);
        } catch (\Exception $e) {
            return redirect('/subjects')->with('message', 'Моля проверете коректността на въведените от Вас данни.');
        }
    }
}
