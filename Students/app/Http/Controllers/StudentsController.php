<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Repositories\CourseRepository;
use App\Repositories\SpecialityRepository;
use App\Repositories\StudentRepository;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    protected $studentRepository;
    protected $courseRepository;
    protected $specialityRepository;

    public function __construct(StudentRepository $studentRepository, CourseRepository $courseRepository, SpecialityRepository $specialityRepository)
    {
        $this->middleware('auth');
        $this->studentRepository = $studentRepository;
        $this->courseRepository = $courseRepository;
        $this->specialityRepository = $specialityRepository;
    }

    private $name;
    private $courseId;
    private $specialityId;

    private function showStudents()
    {
        return $this->studentRepository->addScopeQuery(
            function ($query) {
                return $query->join('specialities', 'students.speciality_id', '=', 'specialities.id')
                    ->join('courses', 'students.course_id', '=', 'courses.id')
                    ->select('students.*', 'courses.name', 'specialities.name_short');
            });
    }

    private function nameFilter()
    {
        return $this->showStudents()->addScopeQuery(function ($query) {
            return $query->where('fname', 'like', '%' . $this->name[0] . '%');
        })->addScopeQuery(function ($query) {
            return $query->where('lname', 'like', '%' . $this->name[1] . '%');
        });
    }

    private function courseFilter()
    {
        return $this->showStudents()->addScopeQuery(function ($query) {
            return $query->where('course_id', 'like', $this->courseId);
        });
    }

    private function specialityFilter()
    {
        return $this->showStudents()->addScopeQuery(function ($query) {
            return $query->where('speciality_id', 'like', $this->specialityId);
        });
    }

    public function index(Request $request)
    {
        $studentName = $request->get('studentName', null);
        if (!is_null($studentName)) {
            $this->name = Helper::separateFullName($studentName);
        }
        $this->courseId = $request->get('courseID', 0);
        $this->specialityId = $request->get('specialityID', 0);
        $perPage = 10;
        $startIndex = (($request->get('page', 1) - 1) * $perPage) + 1;

        $courses = $this->courseRepository->all();
        $allSpecialities = $this->courseRepository->makeModel();
        $allSpecialities['id'] = 0;
        $allSpecialities['name'] = "Всички";
        $courses->push($allSpecialities);

        $specialities = $this->specialityRepository->all();
        $allSpecialities = $this->specialityRepository->makeModel();
        $allSpecialities['id'] = 0;
        $allSpecialities['name'] = "Всички";
        $specialities->push($allSpecialities);

        $students = $this->showStudents()->paginate($perPage);
             //TODO:: find better way to do queries
        if (!empty($request->all())) {
            switch ($request->except('_token')) {
                case is_null($studentName) && ($this->courseId != 0) && ($this->specialityId == 0):
                    $students = $this->courseFilter()->paginate($perPage)->appends($request->only('courseID'));
                    break;
                case is_null($studentName) && ($this->courseId == 0) && ($this->specialityId != 0):
                    $students = $this->specialityFilter()->paginate($perPage)->appends($request->only('specialityID'));
                    break;
                case is_null($studentName) && ($this->courseId != 0) && ($this->specialityId != 0):
                    $students = $this->courseFilter()->addScopeQuery(function ($query) {
                        return $query->where('speciality_id', 'like', $this->specialityId);
                    })->paginate($perPage)->appends($request->only('courseID', 'specialityID'));
                    break;
                case !is_null($studentName) && ($this->courseId == 0) && ($this->specialityId == 0):
                    $students = $this->nameFilter()->paginate($perPage)->appends($request->only('studentName'));
                    break;
                case !is_null($studentName) && ($this->courseId != 0) && ($this->specialityId == 0):
                    $students = $this->courseFilter()->addScopeQuery(function ($query) {
                        return $query->where('fname', 'like', '%' . $this->name[0] . '%');
                    })->addScopeQuery(function ($query) {
                        return $query->where('lname', 'like', '%' . $this->name[1] . '%');
                    })->paginate($perPage)->appends($request->only('courseID','studentName'));
                    break;
                case !is_null($studentName) && ($this->courseId == 0) && ($this->specialityId != 0):
                    $students = $this->specialityFilter()->addScopeQuery(function ($query) {
                        return $query->where('fname', 'like', '%' . $this->name[0] . '%');
                    })->addScopeQuery(function ($query) {
                        return $query->where('lname', 'like', '%' . $this->name[1] . '%');
                    })->paginate($perPage)->appends($request->only('specialityID','studentName'));
                    break;
                case !is_null($studentName) && ($this->courseId != 0) && ($this->specialityId != 0):
                    $students = $this->courseFilter()->addScopeQuery(function ($query) {
                        return $query->where('speciality_id', 'like', $this->specialityId);
                    })->addScopeQuery(function ($query) {
                        return $query->where('fname', 'like', '%' . $this->name[0] . '%');
                    })->addScopeQuery(function ($query) {
                        return $query->where('lname', 'like', '%' . $this->name[1] . '%');
                    })->paginate($perPage)->appends($request->all());
                    break;
                default:
                    $students = $this->showStudents()->paginate($perPage)->appends($request->all());;
                    break;
            }
        }
        return View('students.index', compact('students', 'specialities', 'courses', 'startIndex'));
    }
}
