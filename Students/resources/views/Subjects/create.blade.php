@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row">
            @if (\Session::has('message'))
                <div class="alert alert-info">{{\Session::get('message') }}</div>
            @endif

            <div class="row col-6 text-center">
                <form action="{{url('subjects')}}" method="post">
                    {{csrf_field()}}
                    <fieldset>
                        <div>
                            <legend>Добавяне на дисциплина</legend>
                            <label for="name" style="margin-left: 134px;">Име: </label>
                            <input id="name" name="name" size="30" maxlength="50" required value="{{ old('name') }}"/>
                            <br/>
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        </div>

                        <div>
                            <label for="workload_lectures">Хорариум (Л): </label>
                            <input id="workload_lectures" name="workload_lectures" type="text" required
                                   value="{{ old('workload_lectures') }}"/>
                            <br/>
                            <small class="text-danger">{{ $errors->first('workload_lectures') }}</small>
                        </div>

                        <div>
                            <label for="workload_exercises">Хорариум (У): </label>
                            <input id="workload_exercises" name="workload_exercises" type="text" required
                                   value="{{ old('workload_exercises')}}">
                            <br/>
                            <small class="text-danger">{{ $errors->first('workload_exercises') }}</small>
                        </div>

                        <div>
                            <a href="{{ URL::to('subjects')}}" class="btn btn-warning">Откажи</a>
                            <input type="submit" class="btn btn-primary" value="Добави"/>
                            &nbsp;
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection