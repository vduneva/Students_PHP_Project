<?php

namespace App\Repositories;

use App\Models\Subject;
use Torann\LaravelRepository\Repositories\AbstractRepository;

class SubjectRepository  extends AbstractRepository
{
   protected $model = Subject::class;
}