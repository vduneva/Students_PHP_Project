@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row col-6 text-center">
            <form action="{{action('StudentsController@index')}}" method="get">
                {{ csrf_field() }}
                <fieldset>
                    <div style="margin-left: 42px;">
                        <label for="studentName">Име: </label>
                        <input id="studentName" name="studentName" type="text" size="30"/> <button type="submit" class="search-btn btn btn-sm">
                            <span class="glyphicon glyphicon-search"></span>
                        </button><br/>
                    </div>

                    <div style="margin-right: 160px;">
                        <label for="courseID">Курс: </label>
                        <select id="courseID" name="courseID">

                            @foreach ($courses->sortBy('id') as $course)
                                <option value="{{ $course->id }}">
                                    {{ $course->name }}
                                </option>
                            @endforeach

                        </select><br/>
                    </div>

                    <div style="margin-right: 114px;">
                        <label for="specialityID">Специалност: </label>
                        <select id="specialityID" name="specialityID">

                            @foreach ($specialities->sortBy('id') as $speciality)
                                <option value="{{ $speciality->id }}">
                                    {{ $speciality->name }}
                                </option>
                            @endforeach

                        </select><br/>
                    </div>
                </fieldset>
            </form>
        </div>

        <br class="clearfix"/>
        <div class="row col-5">
            @if ($students->count() > 0)
                <table class="table table-stripped table-responsive table-bordered">
                    <thead class="th-dark justify-content-center">

                    <tr>
                        <td>№</td>
                        <td>Име</td>
                        <td>E-mail</td>
                        <td>Ф. Номер</td>
                        <td>Курс</td>
                        <td>Специалност</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($students as $student)
                        <tr>
                            <td>{{ $startIndex }}</td>
                            <td>{{ $student->fname }} {{ $student->lname }}</td>
                            <td>{{ $student->email }}</td>
                            <td>{{$student->fnumber}}</td>
                            <td>{{$student->name}}</td>
                            <td>{{$student->name_short}}</td>
                        </tr>
                        @php($startIndex++)
                    @endforeach
                    @else
                        <div class="row col-6 text-center">Няма намерени резултати! <a href="{{url('students/')}}">
                                <span class="glyphicon glyphicon-circle-arrow-left"></span></a></div>
                    @endif
                    </tbody>
                </table>
                @if( $students->count() > 0)
                    {{ $students->links() }}
                @endif
        </div>
    </div>
@endsection
