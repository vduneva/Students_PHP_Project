@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row">
            @if (\Session::has('message'))
                <div class="alert alert-info">{{\Session::get('message') }}</div>
            @endif

            <div class="row col-6 text-center">
                <form action="{{url('assessments')}}" method="post">
                    {{csrf_field()}}
                    <fieldset>
                        <div>
                            <legend>Добавяне на оценка</legend>
                            <label for="studentName" style="margin-left: 116px;">Студент: </label>
                            <input id="studentName" list="students" name="studentFN" size="30" required value="{{ old('studentFN') }}"/>
                            <datalist id="students">
                                @foreach ($students as $student)
                                    <option value="{{($student->fnumber)}}">
                                        {{ $student->fname .' ' .$student->lname}}
                                    </option>
                                @endforeach
                            </datalist>
                            <br/>
                            <small class="text-danger">{{ $errors->first('studentFN') }}</small>
                        </div>

                        <div>
                            <label for="subject_id">Дисциплина: </label>
                            <select id="subject_id" name="subject_id" style="margin-right: 45px">
                                @foreach ($subjects as $subject)
                                    <option value="{{ $subject->id }}">
                                        {{ $subject->name }}
                                    </option>
                                @endforeach
                            </select><br/>
                            &nbsp;
                        </div>

                        <div>
                            <label for="workload_lectures">Хорариум (Л): </label>
                            <input id="workload_lectures" name="workload_lectures" type="text" required value="{{ old('workload_lectures') }}"/><br/>
                            <small class="text-danger">{{ $errors->first('workload_lectures') }}</small>
                        </div>

                        <div>
                            <label for="workload_exercises">Хорариум (У): </label>
                            <input id="workload_exercises" name="workload_exercises" type="text" required  value="{{ old('workload_exercises') }}"/><br/>
                            <small class="text-danger">{{ $errors->first('workload_exercises') }}</small>
                        </div>

                        <div>
                            <label for="assessment"  style="margin-left: 40px;">Оценка: </label>
                            <input id="assessment" name="assessment" type="text" required value="{{ old('assessment') }}"/><br/>
                            <small class="text-danger">{{ $errors->first('assessment') }}</small>
                        </div>

                        <div style="margin-left: 350px;">
                            <a href="{{ URL::to('assessments')}}" class="btn btn-warning">Откажи</a>
                            <input type="submit" class="btn btn-primary" value="Добави"/>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection