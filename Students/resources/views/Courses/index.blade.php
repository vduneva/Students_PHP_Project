@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row">

            <div class="container">
                @if (\Session::has('message'))
                    <div class="alert alert-info">{{\Session::get('message') }}</div>
                @endif

                <div class="row" style="float: right">
                    <a class="icon-btn btn-success non-decorated " href="{{url('courses/create/')}}">
                        <span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-success"></span>
                        Добави
                    </a>
                </div>
            </div>
        </div>

        <div class="row col-6 text-center">
            <form action="{{action('CoursesController@index')}}" method="get">
                {{ csrf_field() }}
                <fieldset>
                    <input name="courseName" type="text" size="30" class="rounded"
                           placeholder=" Търсене"/>
                    <button type="submit" class="search-btn btn btn-sm">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                </fieldset>
            </form>
        </div>

        <br class="clearfix"/>
        <div class="row col-5">

            @if  ($courses->count() > 0)
                <table class="table table-stripped table-responsive table-bordered">
                    <thead class="justify-content-center th-dark">
                    <tr>
                        <td>№</td>
                        <td>Име</td>
                        <td>Операции</td>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($courses->sortBy('name') as $course)
                        <tr>
                            <td>{{ $startIndex }}</td>
                            <td>{{ $course->name  . ' курс'}}</td>
                            <td>
                                <a href="{{ URL::to('courses/' .$course->id  .'/edit')}}">
                                    <button class="edit">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </button>
                                </a>

                                <form action="{{action('CoursesController@destroy', $course->id )}}" method="post"
                                      class="delete_form">
                                    {{csrf_field()}}
                                    <input name="_method" type="hidden" value="DELETE"/>
                                    <button type="submit" class="delete">
                                        <span class="glyphicon glyphicon-trash" style="color: red"></span>
                                    </button>
                                </form>
                            </td>
                        </tr>
                        @php($startIndex++)
                    @endforeach
                    @else
                        <div class="row col-6 text-center">Няма намерени резултати!
                            <a href="{{url('courses/')}}">
                                <span class="glyphicon glyphicon-circle-arrow-left"></span></a></div>
                    @endif
                    </tbody>
                </table>

                @if( $courses->count() > 0)
                    {{ $courses->links() }}
                @endif
        </div>
    </div>
@endsection


