<?php

namespace App\Repositories;

use App\Models\Course;
use Torann\LaravelRepository\Repositories\AbstractRepository;

class CourseRepository  extends AbstractRepository
{
    protected $model = Course::class;
}
