<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Repositories\CourseRepository;
use App\Repositories\StudentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CoursesController extends Controller
{
    protected $courseRepository;
    protected $studentRepository;

    public function __construct(CourseRepository $courseRepository, StudentRepository $studentRepository)
    {
        $this->middleware('auth');
        $this->courseRepository = $courseRepository;
        $this->studentRepository = $studentRepository;
    }

    private function rules($id)
    {
        return [
            'name' => 'required|max:20|unique:courses,name,' . $id
        ];
    }

    private $errors = [
        'name.required' => "Моля попълнете полето.",
        'name.unique' => 'Името се използва.',
        'name.max' => 'Дължината на името не трябва да е повече от 20 символа.'
    ];
    private $name;

    public function index(Request $request)
    {
        $courseName = $request->get('courseName', null);
        $perPage = 4;
        $startIndex = (($request->get('page', 1) - 1) * $perPage) + 1;
        $courses = $this->courseRepository->paginate($perPage);

        if (!is_null($courseName)) {
            $this->name = $courseName;
            $courses = $this->courseRepository->addScopeQuery(function ($query) {
                return $query->where('name', 'like', $this->name . '%');
            })->paginate($perPage)->appends($request->only('courseName'));
        }
        return view('courses.index', compact('courses', 'startIndex'));
    }

    public function create()
    {
        return view('courses.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules($request->get('id')), $this->errors);

        if ($validator->fails()) {
            return redirect('courses/create')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            try {
                $this->courseRepository->create($request->all());
                return redirect('courses')->with('message', 'Успешно добавихте нов курс.');
            } catch (\Exception $е) {
                return redirect('courses/create')->with('message', 'Моля проверете коректността на въведените от Вас данни.');
            }
        }
    }

    public function edit($id)
    {
        $course = $this->courseRepository->findBy('id', $id, ['*']);

        if (is_null($course)) {
            return redirect('/courses');
        }
        return view('courses.edit', compact('course'));
    }

    public function update(Request $request, $id)
    {
        $course = $this->courseRepository->findBy('id', $id, ['*']);
        if (is_null($course)) {
            return redirect('/courses');
        }

        $validator = Validator::make($request->all(), $this->rules($id), $this->errors);
        if ($validator->fails()) {
            return redirect('courses/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            try {
                $differences = 0;
                if ($course['name'] != $request->get('name')) {
                    $differences++;
                }
                if ($differences > 0) {
                    $this->courseRepository->update($course, $request->all());
                    return redirect('/courses')->with('message', 'Успешно обновихте информацията за ' . $course['name'] . ' курс');
                } else {
                    return redirect('/courses');
                }
            } catch (\Exception $е) {
                return redirect('courses/' . $id . '/edit')->with('message', 'Моля проверете коректността на въведените от Вас данни.')->withInput($request->all());
            }
        }
    }

    public function destroy($id)
    {
        try {
            $course = $this->courseRepository->findOrFail($id, ['*']);
            $students = $this->studentRepository->findWhere(['course_id' => $id], ['id'], 'like');
            $name = $course['name'];

            if ($students->count() > 0) {
                return redirect('/courses')->with('message', $name . '  курс се използва и не може да бъде изтрит.');
            }

            $this->courseRepository->delete($course);
            return redirect('/courses')->with('message', 'Успешно изтрихте курс ' . $name);
        } catch (\Exception $e) {
            return redirect('/courses')->with('message', 'Моля проверете коректността на въведените от Вас данни.');
        }
    }
}

