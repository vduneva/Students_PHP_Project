<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Repositories\CourseRepository;
use App\Repositories\SpecialityRepository;
use App\Repositories\StudentRepository;
use App\Repositories\SubjectRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $studentRepository;
    protected $courseRepository;
    protected $specialityRepository;
    protected $subjectRepository;

    public function __construct(StudentRepository $studentRepository, CourseRepository $courseRepository,
                                SpecialityRepository $specialityRepository, SubjectRepository $subjectRepository)
    {
        $this->middleware('auth');
        $this->studentRepository = $studentRepository;
        $this->courseRepository = $courseRepository;
        $this->specialityRepository = $specialityRepository;
        $this->subjectRepository = $subjectRepository;
    }

    private $name;
    private $courseId;
    private $specialityId;

    public function index(Request $request)
    {
        $studentName = $request->get('studentName', null);
        if (!is_null($studentName)) {
            $this->name = Helper::separateFullName($studentName);
        }
        $this->courseId = $request->get('courseID', 0);
        $this->specialityId = $request->get('specialityID', 0);
        $perPage = 10;
        $startIndex = (($request->get('page', 1) - 1) * $perPage) + 1;

        $courses = $this->courseRepository->all();
        $allCourses = $this->courseRepository->makeModel();
        $allCourses['id'] = 0;
        $allCourses['name'] = "Всички";
        $courses->push($allCourses);

        $specialities = $this->specialityRepository->all();
        $allSpecialities = $this->specialityRepository->makeModel();
        $allSpecialities['id'] = 0;
        $allSpecialities['name'] = "Всички";
        $allSpecialities['name_short'] = "В";
        $specialities->push($allSpecialities);

        $subjects = $this->subjectRepository->all();

        $query = DB::table('students')
            ->join('students_assessments', 'students.id', '=', 'students_assessments.student_id')
            ->join('specialities', 'students.speciality_id', '=', 'specialities.id')
            ->join('courses', 'students.course_id', '=', 'courses.id')
            ->join('subjects', 'students_assessments.subject_id', '=', 'subjects.id')
            ->select('students.*', 'courses.name', 'specialities.name_short',
                DB::raw('GROUP_CONCAT(students_assessments.workload_lectures ORDER BY subject_id ASC) AS lectures_s'),
                DB::raw('GROUP_CONCAT(students_assessments.workload_exercises ORDER BY subject_id ASC) AS exercises_s'),
                DB::raw('GROUP_CONCAT(students_assessments.assessment ORDER BY subject_id ASC) AS assessment'),
                DB::raw('GROUP_CONCAT(subjects.workload_lectures ORDER BY subject_id ASC) AS lectures_sb'),
                DB::raw('GROUP_CONCAT(subjects.workload_exercises ORDER BY subject_id ASC) AS exercises_sb'),
                DB::raw('SUM(students_assessments.workload_lectures) AS lectures_total_s'),
                DB::raw('SUM(students_assessments.workload_exercises) AS exercises_total_s'),
                DB::raw('SUM(subjects.workload_lectures) AS lectures_total_sb'),
                DB::raw('SUM(subjects.workload_exercises) AS exercises_total_sb'),
                DB::raw('AVG(students_assessments.assessment) AS avg_assessment')
            );

        $students = $query->groupBy('students.id')->orderBy('fname', 'asc')->orderBy('lname', 'asc')->paginate($perPage);

        if (!empty($request->all())) {
            switch ($request->except('_token')) {
                case is_null($studentName) && ($this->courseId != 0) && ($this->specialityId == 0):
                    $students = $query->where('course_id', 'like', $this->courseId)->paginate($perPage)->appends($request->only('courseID'));
                    break;
                case is_null($studentName) && ($this->courseId == 0) && ($this->specialityId != 0):
                    $students = $query->where('speciality_id', 'like', $this->specialityId)->paginate($perPage)->appends($request->only('specialityID'));
                    break;
                case is_null($studentName) && ($this->courseId != 0) && ($this->specialityId != 0):
                    $students = $query->where('course_id', 'like', $this->courseId)->where('speciality_id', 'like', $this->specialityId)
                        ->paginate($perPage)->appends($request->only('courseID', 'specialityID'));
                    break;
                case !is_null($studentName) && ($this->courseId == 0) && ($this->specialityId == 0):
                    $students = $query->where('fname', 'like', '%' . $this->name[0] . '%')->where('lname', 'like', '%' . $this->name[1] . '%')
                        ->paginate($perPage)->appends($request->only('studentName'));
                    break;
                case !is_null($studentName) && ($this->courseId != 0) && ($this->specialityId == 0):
                    $students = $query->where('course_id', 'like', $this->courseId)->where('fname', 'like', '%' . $this->name[0] . '%')->
                    where('lname', 'like', '%' . $this->name[1] . '%')->paginate($perPage)->appends($request->only('courseID', 'studentName'));
                    break;
                case !is_null($studentName) && ($this->courseId == 0) && ($this->specialityId != 0):
                    $students = $query->where('speciality_id', 'like', $this->specialityId)->where('fname', 'like', '%' . $this->name[0] . '%')->
                    where('lname', 'like', '%' . $this->name[1] . '%')->paginate($perPage)->appends($request->only('specialityID', 'studentName'));
                    break;
                case !is_null($studentName) && ($this->courseId != 0) && ($this->specialityId != 0):
                    $students = $query->where('course_id', 'like', $this->courseId)->where('speciality_id', 'like', $this->specialityId)
                        ->where('fname', 'like', '%' . $this->name[0] . '%')->where('lname', 'like', '%' . $this->name[1] . '%')
                        ->paginate($perPage)->appends($request->all());
                    break;
                default:
                    $students = $query->groupBy('students.id')->orderBy('fname', 'asc')->orderBy('lname', 'asc')
                        ->paginate($perPage)->appends($request->all());;
                    break;
            }
        }
        return View('home', compact('students', 'specialities', 'courses', 'subjects', 'startIndex'));
    }
}

