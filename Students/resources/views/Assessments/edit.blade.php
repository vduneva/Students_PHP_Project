@extends('layouts.app')

@section('content')
    <div class="container-fluid" style="padding-left: 50px;">
        <div class="row">
            @if (\Session::has('message'))
                <div class="alert alert-info">{{\Session::get('message') }}</div>
            @endif

            <div class="row col-6 text-center">
                <form action="{{action('StudentAssessmentsController@update',$assessment->id)}}" method="post">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="PATCH"/>
                    <fieldset>
                        <legend>Редактиране на оценка</legend>
                        <div>
                            <label for="studentName" style="margin-left: 130px;">Име: </label>
                            <input id="studentName" name="studentName" type="text" size="30" readonly
                                   value="{{$assessment->student->fname.' ' .$assessment->student->lname}}"/>
                            <input type="hidden" name="studentFN" value="{{$assessment->student->fnumber}}" readonly/>
                            <small class="text-danger">{{ $errors->first('studentFN') }}</small>
                            <br/>
                        </div>

                        <div>
                            <label for="subjectName" style="margin-left: 75px;">Дисциплина: </label>
                            <input id="subjectName" type="text" size="30" readonly
                                   value="{{$assessment->subject->name}}"/>
                            <input type="hidden" name="subject_id" value="{{$assessment->subject->id}}" readonly/>
                            <small class="text-danger">{{ $errors->first('subject_id') }}</small>
                            <br/>
                        </div>

                        <div>
                            <label for="workload_lectures">Хорариум (Л): </label>
                            <input id="workload_lectures" name="workload_lectures" type="text"
                                   value="{{$assessment->workload_lectures}}" required/><br/>
                            <small class="text-danger">{{ $errors->first('workload_lectures') }}</small>
                        </div>

                        <div>
                            <label for="workload_exercises">Хорариум (У): </label>
                            <input id="workload_exercises" name="workload_exercises" type="text"
                                   value="{{$assessment->workload_exercises}}" required/><br/>
                            <small class="text-danger">{{ $errors->first('workload_exercises') }}</small>
                        </div>

                        <div>
                            <label for="assessment" style="margin-left: 40px;">Оценка: </label>
                            <input id="assessment" name="assessment" type="text" value="{{$assessment->assessment}}"
                                   required/><br/>
                            <small class="text-danger">{{ $errors->first('assessment') }}</small>
                        </div>

                        <div style="margin-left: 350px;">
                            <a href="{{ URL::to('assessments')}}" class="btn btn-warning">Откажи</a>

                            <input type="submit" class="btn btn-primary" value="Редактирай"/>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
@endsection